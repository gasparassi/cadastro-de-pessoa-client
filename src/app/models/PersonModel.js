/**
 * Person Model
 */

export default class PersonModel {

  id = null;
  first_name = "";
  last_name = "";
  email = "";
  phone = "";
  pessoa_fisica = false;
  cpf = "";
  cnpj = "";

  constructor(first_name, last_name, email, phone, pessoa_fisica, cpf, cnpj) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone = phone;
    this.pessoa_fisica = pessoa_fisica;
    this.cpf = cpf;
    this.cnpj = cnpj;
  }

}
