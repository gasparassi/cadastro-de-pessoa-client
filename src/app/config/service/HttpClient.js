import axios from 'axios';

/**
 * @typedef {HttpClient}
 */
export default class HttpClient {
	baseUrl = 'http://localhost:8000';
	timeOut = 10000;
	standard = null;

	constructor() {
		this.standard = axios.create({
			baseURL: this.baseUrl,
			timeout: this.timeOut,
			transformResponse: [
				function(data) {
					return data;
				}
			]
		});
  }
}
